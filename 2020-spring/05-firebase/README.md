# 챕터 5. 서버에 사진과 정보 저장하기

* [01. Firebase 호스팅](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/01/README.md)
* [02. Firebase 데이터베이스](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/02/README.md)
* [03. Firebase 스토리지](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/03/README.md)
* [04. Firebase 정보와 함께 사진 올리기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/04/README.md)