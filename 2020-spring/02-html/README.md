# 챕터 2. 웹 사이트 뼈대 만들기

* [04. HTML 기본 태그 1](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/04/README.md)
* [05. HTML 기본 태그 2](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/05/README.md)
* [06. HTML 중첩되는 태그](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/06/README.md)
* [07. HTML 인라인 요소와 블럭 요소](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/07/README.md)
* [08. HTML 구획과 시맨틱 요소](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/08/README.md)
* [09. HTML 폼 사용하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/02-html/09/README.md)