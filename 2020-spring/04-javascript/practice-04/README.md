# Practice 04. 썸네일 나타내고 좋아요 토글하기

<a href="https://fastcampus-all.netlify.com/2020-spring/04-javascript/practice-05" target="_blank">이번강 완성본 보기</a>
<br>
<a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/04-javascript/practice-04/practice-04.zip" target="_blank">
  [실습파일 다운로드 받기]
</a>
<br>
<br>

## data 내용에 따라 갤러리에 썸네일들을 넣으려면?

### 방법 1. 각 `element`를 생성해서 `append`하기
* `article` 요소 생성 뒤 내부 요소들을 생성해 `append`
> * 구조가 복잡한 경우 코딩 및 분석이 어려움 

<br>

### 방법 2. 복사용 `article` 요소 활용하기
* 숨겨진(`display: none`) 요소를 하나 마련
* 이를 `cloneNode` 한 뒤 숨김을 풀고 사진 내용을 적용

> 복사용 요소
```html
<article class="hidden">
  <div class="photo"></div>
  <div class="info">
    <div class="like"></div>
    <div class="author"></div>
    <div class="desc">
    </div>
  </div>
</article>
```

<br>

***

<br>

## `showPhotos` 함수

```javascript
function showPhotos () {

  // 갤러리 div 선택
  var gallery = document.querySelector("#gallery");

  photos.forEach(function (photo) {
    // 각 사진을 썸네일로 만들어 넣음
  })
}
```

<br>

> `init` 함수에서 `showPhotos` 실행시키기
```javascript
  showPhotos();
```

<br>

> 사진마다 `article` 복사해 내용 채우기
```javascript
// 마련한 요소 복사하여 숨김 풀기
var photoNode = document.querySelector("article.hidden").cloneNode(true);
photoNode.classList.remove("hidden");

// 사진의 내용 채우기
photoNode.querySelector(".author").innerHTML = photo.user_name;
photoNode.querySelector(".desc").innerHTML = photo.description;
photoNode.querySelector(".like").innerHTML = photo.likes;
photoNode.querySelector(".photo").style.backgroundImage 
  = "url('./img/photo/" + photo.file_name + "')";

// 좋아요 여부 클래스로 표시
if (my_info.like.indexOf(photo.idx) > -1) {
  photoNode.querySelector(".like").classList.add("on");
}

// 갤러리 요소에 붙여넣기
gallery.append(photoNode);
```

<br>

> 함수 시작시 현존하는 썸네일들 삭제  
> * `showPhotos` 함수가 재실행될 경우 대비
```javascript
var existingNodes 
  = document.querySelectorAll("#gallery article:not(.hidden)");
existingNodes.forEach(function (existingNode) {
  existingNode.remove();
});
```

<br>

***

<br>

## 썸네일의 `좋아요`를 토글하려면?

* `my_info.like`에 해당 `idx`가 없다면  
    * `my_info.like`에 해당 `idx`를 넣는다.
    * 해당 사진의 `likes`를 1 증가시킨다.

* `my_info.like`에 해당 `idx`가 있다면  
    * `my_info.like`에서 해당 `idx`를 뺀다.  
    * 해당 사진의 `likes`를 1 감소시킨다.

<br>

***

<br>


## `toggleLike` 함수
```javascript
function toggleLike(idx) {
  if (my_info.like.indexOf(idx) === -1) {

    // 내가 좋아요하지 않은 사진일 때
    // → 좋아요로 바꿈

  } else {

    // 내가 좋아요 한 사진일 때
    // → 좋아요 해제

  }

  // 최종적용
  showPhotos();
}
```

<br>

> 좋아요에 포함시키고 해당 사진의 좋아요 수 올림
```javascript
my_info.like.push(idx);
for (var i = 0; i < photos.length; i++) {
  if (photos[i].idx === idx) {
    photos[i].likes++;
    break;
  }
}
```

<br>

> 좋아요에서 제외하고 해당 사진의 좋아요 수 내림
```javascript
my_info.like = my_info.like.filter(
  function (it) { return it !== idx; }
);
for (var i = 0; i < photos.length; i++) {
  if (photos[i].idx === idx) {
    photos[i].likes--;
    break;
  }
}
```

## showPhotos에 좋아요 버튼 리스너 넣기 포함
```javascript
photoNode.querySelector(".like").addEventListener(
  "click", function () { toggleLike(photo.idx) }
)
```

<br>

***

<br>

## 발전과제

모든 썸네일들을 새로 생성해 넣지 않고 (즉 `showPhotos`를 호출하지 않고)  
각각의 `좋아요`를 토글하려면 어떻게 짜면 될까요?

### 힌트  
* 각 썸네일의 `좋아요` 버튼을 `querySelector`로 선택할 수 있도록 함
* `showPhoto` 함수에서 각 썸네일에 클래스나 아이디를 지정할 수 있음

### 직접 코딩해보세요!

<br>

***

<br>

## 다음 강좌
* [05. 정렬과 필터 적용하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-05/README.md)